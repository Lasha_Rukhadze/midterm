package com.example.quizapp

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query

@Dao
interface UserDao {
    @Insert
    suspend fun insertAll(vararg users: UserEntity)

   /** @Delete
    suspend fun delete(user: UserEntity)
   */

    @Query("SELECT * FROM users")
    suspend fun getAll(): List<UserEntity>
}