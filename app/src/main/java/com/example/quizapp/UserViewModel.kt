package com.example.quizapp

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class UserViewModel : ViewModel() {

    private var userLiveData = MutableLiveData<List<UserEntity>>()

    fun getUserLiveData() : LiveData<List<UserEntity>> {
        return userLiveData
    }

    fun read(){
        viewModelScope.launch {
            withContext(Dispatchers.IO){
                userLiveData.postValue(DataBase.db.userDao().getAll())
            }
        }
    }

    fun write(name : String, password : String, recordPoint : Int){
        val user = UserEntity(name, password, recordPoint)
        viewModelScope.launch {
            withContext(Dispatchers.IO){
                DataBase.db.userDao().insertAll(user)
            }
        }
    }
}