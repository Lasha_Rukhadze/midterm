package com.example.quizapp

import androidx.room.Room

object DataBase {
    val db : UserDatabase by lazy {
        Room.databaseBuilder(
            App.context!!,
            UserDatabase::class.java, "users_database"
        ).build()
    }
}