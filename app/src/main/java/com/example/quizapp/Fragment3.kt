package com.example.quizapp

import android.app.ProgressDialog
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import com.example.quizapp.databinding.Fragment3Binding
import kotlin.Result

class Fragment3 : Fragment() {

    lateinit var binding : Fragment3Binding
    private val gameViewModel: GameViewModel by viewModels()
    private var mainList = mutableListOf<MainModel>()

    private val args : Fragment3Args by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = Fragment3Binding.inflate(inflater, container, false)

        return binding.root
    }

    private fun observes(){
        gameViewModel.getGameLiveData().observe(viewLifecycleOwner, { it ->
            when(it.status){
                com.example.quizapp.Result.Status.SUCCESS -> {
                    it.data?.let {
                        mainList.addAll(it.toMutableList())
                    }
                }
                com.example.quizapp.Result.Status.ERROR -> {
                    Toast.makeText(requireActivity(), it.msg, Toast.LENGTH_LONG).show()
                }
                com.example.quizapp.Result.Status.LOADING -> {
                    // ProgressBar implementation
                }
            }
        })
    }


}