package com.example.quizapp

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import com.example.quizapp.databinding.Fragment1Binding

class Fragment1 : Fragment() {

    lateinit var binding : Fragment1Binding
    private lateinit var topAnimation : Animation
    private lateinit var bottomAnimation: Animation

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = Fragment1Binding.inflate(inflater, container, false)

        screenAnimation()
        binding.startIcon.setOnClickListener{
            Navigation.findNavController(binding.root).navigate(R.id.fragment1_to_fragment2)
        }
        return binding.root
    }

    private fun screenAnimation(){
        topAnimation = AnimationUtils.loadAnimation(this.context, R.anim.top_animation)
        bottomAnimation = AnimationUtils.loadAnimation(this.context, R.anim.bottom_animation)

        binding.apply {
            quizBanner1.animation = topAnimation
            quizBanner2.animation = topAnimation
            startTitle.animation = bottomAnimation
            startIcon.animation = bottomAnimation
        }
    }
}