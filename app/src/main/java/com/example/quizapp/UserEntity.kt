package com.example.quizapp

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "users")
class UserEntity (
    @PrimaryKey(autoGenerate = true) var id : Int,
    @ColumnInfo(name = "userName") var name : String,
    @ColumnInfo(name = "password") var password : String,
    @ColumnInfo(name = "recordPoint") var recordPoint : Int
){
    constructor(name : String, password: String, recordPoint: Int) : this(0, name, password, recordPoint)


}