package com.example.quizapp

import retrofit2.Response
import retrofit2.http.GET

interface RetrofitRepository {
    @GET("/api.php?amount=25&type=multiple")
    suspend fun getData() : Response<List<MainModel>>
}