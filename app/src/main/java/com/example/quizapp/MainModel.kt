package com.example.quizapp

import com.google.gson.annotations.SerializedName

data class MainModel(
    @SerializedName("response_code")
    val responseCode : Int,
    val results : List<QuestionsModel>) {
}
