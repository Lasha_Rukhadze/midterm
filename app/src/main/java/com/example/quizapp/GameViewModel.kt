package com.example.quizapp

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class GameViewModel : ViewModel() {

    private val gameLiveData = MutableLiveData<Result<List<MainModel>>>()

    fun getGameLiveData() : LiveData<Result<List<MainModel>>> {
        return gameLiveData
    }

    fun init(){
        viewModelScope.launch{
            withContext(Dispatchers.IO){
                getQuestions()
            }
        }
    }

    private suspend fun getQuestions() {
        gameLiveData.postValue(Result.loading())
        val response = RetrofitService.retrofitService().getData()
        if (response.isSuccessful) {
            val data = response.body()
            gameLiveData.postValue(Result.success(data!!))
        } else {
            response.errorBody()
            gameLiveData.postValue(Result.error(response.message()))
        }
    }
}