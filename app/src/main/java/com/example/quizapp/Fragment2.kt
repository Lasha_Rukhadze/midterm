package com.example.quizapp

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.navigation.Navigation
import com.example.quizapp.databinding.Fragment2Binding

class Fragment2 : Fragment() {

    private lateinit var binding: Fragment2Binding
    private val userViewModel : UserViewModel by viewModels()
    private var userList = mutableListOf<UserEntity>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = Fragment2Binding.inflate(inflater, container, false)

        binding.loginButton.setOnClickListener {
            logIn()
        }

        return binding.root
    }

    private fun openFragment3(arg1 : String, arg2 : Int){
        val action = Fragment2Directions.fragment2ToFragment3(arg1, arg2)
        Navigation.findNavController(binding.root).navigate(action)
    }

    private fun logIn() {

        if (binding.username.text.isBlank() || binding.password.text.isBlank()) {
            Toast.makeText(
                this.context,
                "All fields MUST be filled",
                Toast.LENGTH_SHORT
            ).show()
        }
        else {

            observes()

            userList.forEach {
                if (it.name == binding.username.text.toString() && it.password == binding.password.text.toString()) {
                    openFragment3(it.name, it.recordPoint)
                } else if (it.name == binding.username.text.toString() && it.password != binding.password.text.toString()) {
                    Toast.makeText(
                        this.context,
                        "Password is INCORRECT for this username",
                        Toast.LENGTH_SHORT
                    ).show()
                } else {
                    userViewModel.write(binding.username.text.toString(), binding.password.text.toString(), 0)
                    openFragment3(binding.username.text.toString(), 0)
                }
            }
        }
    }

    private fun observes() {
        userViewModel.read()
        userViewModel.getUserLiveData().observe(viewLifecycleOwner, {
            userList.addAll(it)
        })
    }
}